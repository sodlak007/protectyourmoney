$(document).ready(function(){
	$('#nav-icon1').click(function(){
      $(this).toggleClass('open');
      $('.mobile-nav').slideToggle();
    });

  $('.anchor-scroll').anchorScroll({
    offsetTop: 0, // offset for fixed top bars (defaults to 0)
    scrollSpeed: 800 // scroll speed
  });
});  